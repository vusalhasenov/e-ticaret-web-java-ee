package Service;

import Model.UserLogin;

public interface LoginService {

    UserLogin login(String username, String password);

    void sendMail(String email);

    String checkUserName(String username);

    void resetPassword(String pass, String username);

    void checkCode(int code);

}