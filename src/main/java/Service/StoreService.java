package Service;

import Model.EditStore;
import Model.Store;

import java.util.List;

public interface StoreService {

    List<Store> findAll();

    void saveProduct(Store store);

    EditStore findStoreById(Long id);

    void deleteStoreById(Long valueOf);

    void editStore(Long id , Double quantity);
}
