package Service;

import Model.Register;

public interface RegisterService {

    void register(Register register);

    boolean checkUser(String username);

    boolean checkEmail(String email);
}
