package exception;

public class LoginException  extends RuntimeException{

    public LoginException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
