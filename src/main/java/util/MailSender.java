package util;


import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

class MailAuth extends Authenticator {
    private String username;
    private String password;


    public MailAuth(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username , password);
    }
}

public class MailSender {
    private MailSender(){
    }

    public static void sendMail(String toMail, String subject, String text) {
        String username = "vusalhasenov@gmail.com";
        String password = "Vusal19991606";
        MailAuth mailAuth = new MailAuth(username, password);

        try {
            Session session = Session.getInstance(getSmtpProperties(), mailAuth);
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("vusalhasenov@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(toMail));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toMail));
            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
            throw  new IllegalStateException("emaile gonderen zaman xeta bas verdi");
        }
    }

    public static Properties getSmtpProperties() {
        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");
        return prop;
    }


}
