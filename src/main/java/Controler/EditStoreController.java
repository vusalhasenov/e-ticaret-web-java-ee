package Controler;

import Model.EditStore;
import Service.StoreService;
import ServiceImpl.StoreServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/edit-store"})
public class EditStoreController extends HttpServlet {
    private String id;
    private StoreService storeService = new StoreServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        id = req.getParameter("id");
        EditStore editStore = storeService.findStoreById(Long.valueOf(id));
        req.setAttribute("product",editStore);
        req.getRequestDispatcher("editStore.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String storeId = req.getParameter("storeId");
        String quantity = req.getParameter("quantityId");
        storeService.editStore(Long.valueOf(id),Double.valueOf(quantity));
    }
}
