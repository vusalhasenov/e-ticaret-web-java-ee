package Controler;

import Service.LoginService;
import ServiceImpl.LoginServiceImpl;
import exception.LoginException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(urlPatterns = {"/forgot-password"})
public class ForgotPassword extends HttpServlet {
    public static String username;
    LoginService loginService = new LoginServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("forgotPassword.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        username = req.getParameter("username");
        try {
            String email = loginService.checkUserName(username);
            loginService.sendMail(email);
            resp.sendRedirect("check-code");


        }catch (LoginException ex){
            req.setAttribute("error",ex.getMessage());
            req.getRequestDispatcher("forgotPassword.jsp").forward(req,resp);
        }
    }
}
