package Controler;

import Service.LoginService;
import ServiceImpl.LoginServiceImpl;
import exception.LoginException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/check-code"})
public class MailCodeController extends HttpServlet {

    LoginService loginService =new LoginServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("mailCode.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String code = req.getParameter("code");
        try{
        loginService.checkCode(Integer.valueOf(code));
        resp.sendRedirect("reset-password");
        }catch (LoginException ex){
            req.setAttribute("error",ex.getMessage());
            req.getRequestDispatcher("mailCode.jsp").forward(req,resp);
        }
    }

}
