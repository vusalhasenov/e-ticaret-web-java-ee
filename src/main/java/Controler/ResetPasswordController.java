package Controler;

import Service.LoginService;
import ServiceImpl.LoginServiceImpl;
import exception.LoginException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/reset-password"})
public class ResetPasswordController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("resetPassword.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String password = req.getParameter("password");
            String confirmPassword = req.getParameter("confirmPassword");
            if (!password.equals(confirmPassword)) {
                throw new LoginException("password is not comfirmed");
            }
            LoginService loginService = new LoginServiceImpl();
            loginService.resetPassword(password,ForgotPassword.username);
            resp.sendRedirect("login");
        }catch (LoginException ex){
            req.setAttribute("error",ex.getMessage());
            req.getRequestDispatcher("resetPassword.jsp").forward(req,resp);
        }
    }
}
