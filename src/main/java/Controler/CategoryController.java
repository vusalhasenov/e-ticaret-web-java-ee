package Controler;

import Model.Category;
import Service.ProductService;
import ServiceImpl.ProductServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = {"/category"})
public class CategoryController extends HttpServlet {
    ProductService productService = new ProductServiceImpl();
    List<Category> categoryList = productService.allCategory();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("categoryList",categoryList);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("category.jsp");
        requestDispatcher.forward(req,resp);
    }
}
