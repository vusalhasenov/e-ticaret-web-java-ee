package Controler;

import Service.StoreService;
import ServiceImpl.StoreServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/delete-store"})
public class DeleteStoreController extends HttpServlet {
    StoreService storeService = new StoreServiceImpl();
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        storeService.deleteStoreById(Long.valueOf(id));
    }
}
