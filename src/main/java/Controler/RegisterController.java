package Controler;

import Model.Register;
import Service.RegisterService;
import ServiceImpl.RegisterServiceImpl;
import exception.RegisterException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/register"})
public class RegisterController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("register.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      String username = req.getParameter("username");
      String password = req.getParameter("password");
      String confirmPassword = req.getParameter("confirmPassword");
      String name = req.getParameter("name");
      String email = req.getParameter("email");

        Register register = new Register(username,password,confirmPassword,name,email);
        RegisterService registerService = new RegisterServiceImpl();
        try {
            boolean checkUser = registerService.checkUser(username);
            boolean checkEmail = registerService.checkEmail(email);
            registerService.register(register);
            resp.sendRedirect("login");
        }catch (RegisterException ex){
            req.setAttribute("error",ex.getMessage());
            req.getRequestDispatcher("register.jsp").forward(req,resp);
        }
    }
}
