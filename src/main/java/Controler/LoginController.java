package Controler;

import Model.UserLogin;
import Service.LoginService;
import ServiceImpl.LoginServiceImpl;
import exception.LoginException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = {"/login"})
public class LoginController extends HttpServlet {

    private final LoginService loginService = new LoginServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("login.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        try {
           UserLogin userLogin = loginService.login(username, password);
           HttpSession session = req.getSession();
           session.setAttribute("userLogin", userLogin);
           resp.sendRedirect("index");
       } catch (LoginException ex){
           req.setAttribute("error",ex.getMessage());
           req.getRequestDispatcher("login.jsp").forward(req,resp);
       }
    }
}
