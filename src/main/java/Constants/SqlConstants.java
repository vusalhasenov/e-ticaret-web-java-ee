package Constants;

public final class SqlConstants {

    public final static String SAVE_PRODUCT = "INSERT INTO PRODUCT(NAME,PRICE,PRODUCT_TYPE_ID,UNIT_ID) VALUES (?,?,?,?)";

    public final static String FIND_BY_PRODUCT = "SELECT pro.name,pro.price,cat.name,unit.name FROM product pro join category cat \n" +
                                                   "on pro.product_type_id = cat.id join unit on pro.unit_id = unit.id where pro.id=?";



    public final static String FIND_PRODUCT_BY_ID = "select id ,name from product where product_type_id = ?";

    public final static String GET_ALL_STORE = "SELECT st.id , quantity , pr.name, st.is_active, st.insert_date FROM store st join product pr on st.product_id = pr.id where st.is_active=1 order by st.id";

    public final static String FIND_STORE_BY_ID = "select s.id , s.quantity, p.id , p.product_type_id  from store s join product p on s.product_id = p.id  where s.id = ? and s.is_active = 1";

    public final static String DELETE_STORE_BY_ID = "Update store set is_active=0 where id = ?";

    public final static String UPDATE_STORE_BY_ID = "update store set quantity = ? where id =?";

    public final static String SAVE_STORE = "insert into store(quantity,product_id) values(?,?)";

    public final static String GET_ALL_CATEGORY ="select id,name from category";
}
