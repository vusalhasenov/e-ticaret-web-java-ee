package Dao;


import Model.Category;
import Model.Product;

import java.util.List;

public interface ProductDao {

    Product save(Product product);

    Product findProductById(Long id);

    List<Product> findAllProduct(Long id);

    List<Category> allCategory();

    Category findCategoryById(Long id);
}
