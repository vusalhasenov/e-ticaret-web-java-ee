package Dao;

import Model.UserLogin;

import java.util.Optional;

public interface UserLoginDao {

    Optional<UserLogin> findByUsername(String username);

    String getEmail(String username);

    String checkUserName(String username);

    void resetPassword(String encyriptedPass, String username);
}
