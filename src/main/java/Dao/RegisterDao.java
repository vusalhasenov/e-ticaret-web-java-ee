package Dao;

import Model.Register;

public interface RegisterDao {

    void register(Register register);

    boolean checkUser(String username);

    boolean checkEmail(String email);
}
