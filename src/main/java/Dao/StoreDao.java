package Dao;

import Model.EditStore;
import Model.Store;

import java.util.List;

public interface StoreDao {

    List<Store> findAll();

    void saveStore(Store store);

    EditStore findStoreById(Long id);

    void deleteStoreById(Long id);

    void editStore(Long id ,Double quantity);
}
