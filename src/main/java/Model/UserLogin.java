package Model;

import enums.UserType;

import java.io.Serializable;

public class UserLogin implements Serializable {

    private Long id;
    private String username;
    private String password;
    private Integer isActive;
    private Integer userType;
    private UserType userTypeEnum;

    public UserLogin(Long id, String username, String password, Integer userType) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.userType = userType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public UserType getUserTypeEnum() {
        return userTypeEnum;
    }

    public void setUserTypeEnum(UserType userTypeEnum) {
        this.userTypeEnum = userTypeEnum;
    }
}