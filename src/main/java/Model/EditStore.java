package Model;

public class EditStore extends Base {
    private Double quantity;
    private Long productId;
    private Long productTypeId;

    public EditStore() {
    }

    public EditStore(Long id ,Double quantity, Long productId, Long productTypeId) {
        super.id=id;
        this.quantity = quantity;
        this.productId = productId;
        this.productTypeId = productTypeId;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Long productTypeId) {
        this.productTypeId = productTypeId;
    }

    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
    }

    @Override
    public String toString() {
        return "EditStore{" +
                "quantity=" + quantity +
                ", productId=" + productId +
                ", productTypeId=" + productTypeId +
                ", id=" + id +
                '}';
    }
}