package DaoImpl;

import Constants.SqlConstants;
import Dao.ProductDao;
import Model.Category;
import Model.Product;
import Model.Unit;
import connection.DbConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDaoImpl implements ProductDao {


    @Override
    public Product save(Product product) {
        String sql = SqlConstants.SAVE_PRODUCT;

        try(Connection connection = DbConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS)){
                preparedStatement.setString(1,product.getName());
                preparedStatement.setDouble(2,product.getPrice());
                preparedStatement.setLong(3,product.getCategory().getId());
                preparedStatement.setLong(4,product.getUnit().getId());
                int exist =  preparedStatement.executeUpdate();
                if(exist>0){
                    ResultSet resultSet = preparedStatement.getGeneratedKeys();
                    if(resultSet.next()){
                        product.setId(resultSet.getLong(1));
                    }
                }
                connection.commit();
                return product;
        }catch (SQLException ex){
            throw new IllegalStateException("exception happened while adding product to database!");
        }
    }

    @Override
    public Product findProductById(Long id) {
        String sql = SqlConstants.FIND_BY_PRODUCT;
        try (Connection connection = DbConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setLong(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                Category category = new Category(resultSet.getString("cat.name"));
                Unit unit = new Unit(resultSet.getString("unit.name"));
                Product product = new Product(resultSet.getString("pro.name"),resultSet.getDouble("price"),category,unit);
                return product;
            }
            return null;
        }catch(SQLException ex){
            throw new IllegalStateException("System Error");
        }
    }

    @Override
    public List<Product> findAllProduct(Long id) {
        String sql = SqlConstants.FIND_PRODUCT_BY_ID;
        List<Product> productList = new ArrayList<>();
        try(Connection connection = DbConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setLong(1,id);
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()){
                Product product = new Product();
                product.setId(rs.getLong("id"));
                product.setName(rs.getString("name"));
                productList.add(product);
            }
            return productList;
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new IllegalStateException("System Error");
        }
    }

    @Override
    public List<Category> allCategory() {
        List<Category> categoryList = new ArrayList<>();
        String sql = "select id,name from category";
        try (Connection connection = DbConnection.getConnection();){
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                Category category = new Category(rs.getLong("id"),rs.getString("name"));
                categoryList.add(category);
            }
            return categoryList;
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new IllegalStateException("System Error");
        }
    }

    @Override
    public Category findCategoryById(Long id) {
        String sql = "select id, name from category where id = ?";
        try (Connection connection = DbConnection.getConnection();){
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1,id);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()){
                Category category = new Category(rs.getLong("id"),rs.getString("name"));
                return category;
            }
            return null;
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new IllegalStateException("System Error");
        }
    }
}
