package DaoImpl;

import Dao.UserLoginDao;
import Model.UserLogin;
import connection.DbConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class UserLoginDaoImp implements UserLoginDao {
    @Override
    public Optional<UserLogin> findByUsername(String username) {
        String sql = "SELECT id,username,password,is_active,user_type FROM user_login where username = ? and is_active = 1 and user_type = 2";
        try (Connection connection = DbConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)){
             preparedStatement.setString(1,username);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()){
                     UserLogin userLogin = new UserLogin(
                        rs.getLong("id"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getInt("user_type")
                        );
                     return Optional.of(userLogin);
            }
            return  Optional.empty();
        }catch (SQLException ex){
            throw new IllegalStateException("exception happened while getting UserLogin info From Db!");
        }
    }
    @Override
    public String getEmail(String username) {
        String sql = "SELECT email FROM user where user_login_id = (select id from user_login where username = ?";
        try (Connection connection = DbConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String email = resultSet.getString("EMAIL");
                return email;
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalStateException("System Xetasi", e);
        }
    }

    @Override
    public String checkUserName(String username) {
        String sql = "select email from user join user_login on user.user_login_id = user_login.id where username = ?";
        try(Connection connection = DbConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setString(1,username);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return resultSet.getString("email");
            }
            return null;
        }catch (SQLException ex){
            throw new IllegalStateException("System Error");
        }
    }

    @Override
    public void resetPassword(String encyriptedPass, String username) {
        String sql = "update user_login set password = ? where username =?";
        try(Connection connection = DbConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setString(1,encyriptedPass);
            preparedStatement.setString(2,username);
            preparedStatement.executeUpdate();
            connection.commit();
        }catch (SQLException ex){
            throw new IllegalStateException("System Error");
        }
    }
}
