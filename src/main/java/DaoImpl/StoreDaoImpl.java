package DaoImpl;

import Constants.SqlConstants;
import Dao.StoreDao;
import Model.EditStore;
import Model.Store;
import connection.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StoreDaoImpl implements StoreDao {
    @Override
    public List<Store> findAll() {
        String sql = SqlConstants.GET_ALL_STORE;
        List<Store> storeList = new ArrayList<>();
        try(Connection connection = DbConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                Store store = new Store();
                store.setId(rs.getLong("st.id"));
                store.setQuantity(rs.getDouble("quantity"));
                store.setName(rs.getString("pr.name"));
                store.setIsActive(rs.getInt("st.is_active"));
                store.setInsertDate(rs.getDate("st.insert_date"));
                storeList.add(store);
            }
            return storeList;
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new IllegalStateException("System Xetasi!");
        }
    }

    @Override
    public void saveStore(Store store) {
        String sql = SqlConstants.SAVE_STORE;
        try (Connection connection = DbConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)){
                preparedStatement.setDouble(1,store.getQuantity());
                preparedStatement.setLong(2,store.getId());
                preparedStatement.executeUpdate();
                connection.commit();
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new IllegalStateException("System Error");
        }
    }

    @Override
    public EditStore findStoreById(Long id) {
        String sql = SqlConstants.FIND_STORE_BY_ID;
        try(Connection connection = DbConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setLong(1,id);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()){
                EditStore editStore = new EditStore(
                             rs.getLong("id"),
                             rs.getDouble("quantity"),
                             rs.getLong("p.id"),
                             rs.getLong("p.product_type_id"));
                return editStore;
            }
            return null;
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new IllegalStateException("System Error");
        }
    }

    @Override
    public void deleteStoreById(Long id) {
        String sql = SqlConstants.DELETE_STORE_BY_ID;
        try(Connection connection = DbConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setLong(1,id);
            preparedStatement.executeUpdate();
            connection.commit();
        }catch (SQLException ex){
            throw new IllegalStateException("An error occurred when the product was deleted");
        }
    }

    @Override
    public void editStore(Long id, Double quantity) {
        String sql = SqlConstants.UPDATE_STORE_BY_ID;
        try(Connection connection = DbConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setDouble(1,quantity);
            preparedStatement.setLong(2,id);
            preparedStatement.executeUpdate();
            connection.commit();
        }catch (SQLException ex){
            throw new IllegalStateException("Editing time Error occurred");
        }
    }
}
