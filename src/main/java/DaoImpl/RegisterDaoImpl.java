package DaoImpl;

import Dao.RegisterDao;
import Model.Register;
import connection.DbConnection;

import java.sql.*;

public class RegisterDaoImpl implements RegisterDao {
    @Override
    public void register(Register register) {
        String saveUserLogin = "insert into user_login(username,password) values(?,?)";
        String saveUser = "insert into user(first_name,email,user_login_id) values (?,?,?)";
        Connection connection = DbConnection.getConnection();
        try(PreparedStatement psUserLogin = connection.prepareStatement(saveUserLogin, Statement.RETURN_GENERATED_KEYS)){
            PreparedStatement psUser = connection.prepareStatement(saveUser);
            psUserLogin.setString(1,register.getUsername());
            psUserLogin.setString(2,register.getPassword());
            int i = psUserLogin.executeUpdate();
            if(i>0){
                ResultSet rs = psUserLogin.getGeneratedKeys();
                if(rs.next()){
                    psUser.setLong(3,rs.getLong(1));
                }
            }
            psUser.setString(1,register.getName());
            psUser.setString(2,register.getEmail());
            psUser.executeUpdate();
            connection.commit();
        }catch (SQLException ex){
            DbConnection.rollBack(connection);
            throw new IllegalStateException("System Error");
        }finally {
            DbConnection.close(connection);
        }
    }

    @Override
    public boolean checkUser(String username) {
        String sql = "SELECT * FROM user_login where username =?";
        try (Connection connection = DbConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1,username);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return true;
            }
            return false;
        }catch (SQLException ex){
            throw new IllegalStateException("System Error");
        }
    }

    @Override
    public boolean checkEmail(String email) {
        String sql = "SELECT * FROM user where email =?";
        try (Connection connection = DbConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1,email);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                return true;
            }
            return false;
        }catch (SQLException ex){
            throw new IllegalStateException("System Error");
        }
    }
}
