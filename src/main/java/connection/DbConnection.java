package connection;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DbConnection {

    public static Connection getConnection() {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:comp/env");
            DataSource ds = (DataSource) envContext.lookup("jdbc/ecommerceDB");
            Connection connection = ds.getConnection();
            connection.setAutoCommit(false);
            return connection;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new IllegalStateException("exception happened while connection to DB connection");
        }
    }

    public static void close(Connection connection){
        if(connection!= null){
            try {
                connection.close();
            } catch (SQLException ex) {
                throw new IllegalStateException("Connection Baglanan Zaman Xeta Bas Verdi !",ex);
            }
        }
    }

    public static void rollBack(Connection connection){
        try {
            connection.rollback();
        } catch (SQLException ex) {
            throw new IllegalStateException("Connection Rollbak Olan Zaman Xeta Bas Verdi", ex);
        }
    }
}
