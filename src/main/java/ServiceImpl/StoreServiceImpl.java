package ServiceImpl;

import Dao.StoreDao;
import DaoImpl.StoreDaoImpl;
import Model.EditStore;
import Model.Store;
import Service.StoreService;

import java.util.List;

public class StoreServiceImpl implements StoreService {

    StoreDao storeDao = new StoreDaoImpl();
    @Override
    public List<Store> findAll() {
        return storeDao.findAll();
    }

    @Override
    public void saveProduct(Store store) {
            storeDao.saveStore(store);
    }

    @Override
    public EditStore findStoreById(Long id) {
        return storeDao.findStoreById(id);
    }

    @Override
    public void deleteStoreById(Long id) {
         storeDao.deleteStoreById(id);
    }

    @Override
    public void editStore(Long id, Double quantity) {
        storeDao.editStore(id,quantity);
    }
}
