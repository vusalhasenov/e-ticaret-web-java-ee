package ServiceImpl;

import Dao.RegisterDao;
import DaoImpl.RegisterDaoImpl;
import Model.Register;
import Service.RegisterService;
import exception.LoginException;
import exception.RegisterException;
import util.PasswordEncoder;
import validator.LoginValidator;
import validator.RegisterValidator;

public class RegisterServiceImpl implements RegisterService {

    RegisterDao registerDao = new RegisterDaoImpl();
    @Override
    public void register(Register register) {
        if(!register.getPassword().equals(register.getConfirmPassword())){
            throw new RegisterException("password is not confirmed");
        }
        String encyriptedPass = PasswordEncoder.getMd5Password(register.getPassword());
        register.setPassword(encyriptedPass);
        System.out.println(register.getPassword());
        registerDao.register(register);
    }

    @Override
    public boolean checkUser(String username) {
       boolean exist = registerDao.checkUser(username);
       if(exist){
           throw new RegisterException("username already exist!");
       }
       return false;
    }

    @Override
    public boolean checkEmail(String email) {
       boolean exist = registerDao.checkEmail(email);
        if(exist){
            throw new RegisterException("email already exist!");
        }
        return false;
    }
}
