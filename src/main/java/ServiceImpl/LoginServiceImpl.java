package ServiceImpl;

import Dao.UserLoginDao;
import DaoImpl.UserLoginDaoImp;
import Model.UserLogin;
import Service.LoginService;
import exception.LoginException;
import util.AuthenticationUtils;
import util.MailSender;
import util.PasswordEncoder;
import validator.LoginValidator;

import java.util.Random;

public class LoginServiceImpl implements LoginService {

    static int randomPass = new Random().nextInt(9000) + 1000;
    UserLoginDao userLoginDao = new UserLoginDaoImp();
    @Override
    public UserLogin login(String username, String password) {
        LoginValidator.validate(username,password);
        UserLogin userLogin = userLoginDao.findByUsername(username.trim())
                .orElseThrow(()-> new LoginException("invalid username or password"));
        if(!AuthenticationUtils.checkPassword(userLogin.getPassword(),password.trim())){
            throw new LoginException("invalid username or password");
        }
        return userLogin;
    }

    @Override
    public void sendMail(String email) {
        if (!email.equals(email)) {
            throw new LoginException("Mail Duzgun Deyil!");
        }
        MailSender.sendMail(email, "Forgot Password", "Code is :" + randomPass);
    }

    @Override
    public String checkUserName(String username) {
        String getEmail = userLoginDao.checkUserName(username);
        if(getEmail==null){
            throw new LoginException("username not found!");
        }
        return getEmail;
    }

    @Override
    public void resetPassword(String pass, String username) {
        String encyriptedPass = PasswordEncoder.getMd5Password(pass);
        userLoginDao.resetPassword(encyriptedPass,username);
    }

    @Override
    public void checkCode(int code) {
        if (code != randomPass) {
            throw new LoginException("Daxil Etdiyiniz Sifre Yanlisdir");
        }
    }
}
