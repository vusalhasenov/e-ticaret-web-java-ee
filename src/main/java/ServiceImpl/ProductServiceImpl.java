package ServiceImpl;

import Dao.ProductDao;
import DaoImpl.ProductDaoImpl;
import Model.Category;
import Model.Product;
import Service.ProductService;

import java.util.List;

public class ProductServiceImpl implements ProductService {

    ProductDao productDao = new ProductDaoImpl();
    @Override
    public Product saveProduct(Product product) {
        return productDao.save(product);
    }

    @Override
    public Product findProductById(Long id) {
        return productDao.findProductById(id);
    }

    @Override
    public List<Product> allProduct(Long id) {
        return productDao.findAllProduct(id);
    }

    @Override
    public List<Category> allCategory() {
        return productDao.allCategory();
    }

    @Override
    public Category findCategoryById(Long id) {
        return productDao.findCategoryById(id);
    }
}
