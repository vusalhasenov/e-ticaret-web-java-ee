package enums;

import java.util.HashMap;
import java.util.Map;

public enum  UserType {

    ENABLED(1),
    DISABLED(0);

    private final int value;

    public static final Map<Integer,UserType> VALUE_MAP = new HashMap<>();

    static {
        for (UserType userType : values())
            VALUE_MAP.put(userType.value,userType);
    }

    UserType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static UserType getStatus(Integer status){
        return VALUE_MAP.get(status);
    }
}

