package enums;

import java.util.HashMap;
import java.util.Map;

public enum  Status {
    ENABLED(1),
    DISABLED(0);

    private final int value;

    public static final Map<Integer,Status> VALUE_MAP = new HashMap<>();

    static {
        for (Status status : values())
            VALUE_MAP.put(status.value,status);
    }

    Status(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Status getStatus(Integer status){
        return VALUE_MAP.get(status);
    }
}
