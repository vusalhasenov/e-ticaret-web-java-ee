<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="template/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="template/css/simple-sidebar.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
   <c:import url="items/menu.jsp"></c:import>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        </nav>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            </div>
        </nav>
        <div class="card">
            <div class="card-header">
                <button type="button"  onclick="openStoreModal()" class="btn btn-success">ADD PRODUCT</button>
                <a href="logout"><button  style="float: right" type="button" class="btn btn-success">Log Out</button></a>
                <span style="float: right;margin-right: 25px">${sessionScope.userLogin.username}</span>
            </div>
            <div class="card-body">
                <div class="container-fluid">
                    <div id="storeTableId"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="template/js/jquery.min.js"></script>
<script src="template/js/bootstrap.bundle.min.js"></script>
<script src="resource/js/main.js"></script>

<!-- Menu Toggle Script -->
<script>

    $(function (){
        loadProductType('categoryId')
    })


    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>

</body>
<!-- Modal -->
<div class="modal fade" id="storeModalId" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <c:import url="saveProduct.jsp"></c:import>
</div>
    </div>
</div>

<div class="modal fade" id="storeEditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="storeEditModalId">
            <c:import url="editStore.jsp"></c:import>
        </div>
    </div>
</div>

<div class="modal" id="deleteModalId" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content" <%--id="editStoreModalContentId--%>>
            <c:import url="delete.jsp"></c:import>
        </div>
    </div>
</div>
</html>