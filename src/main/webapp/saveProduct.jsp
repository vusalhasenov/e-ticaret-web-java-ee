<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 1/30/2021
  Time: 7:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">ADD PRODUCT</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <label for="categoryId" class="form-label">Category</label>
    <select class="form-control" id="categoryId" onchange="loadProduct($('#categoryId').val(),'productId',false)" ></select>
    <div class="mb-3">
        <label for="productId" class="form-label">Product</label>
        <select class="form-control" id="productId" ></select>
        <label for="quantityId"  class="form-label">Quantity</label>
        <input type="number" class="form-control" id="quantityId" placeholder="quantity">
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="saveStoreId" onclick="saveStore()">Save</button>
        </div>
    </div>
</div>