<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 2/14/2021
  Time: 2:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>login Page</title>
</head>
<style>

    body {
        font-family: "Lato", sans-serif;
    }

    .main-head{
        height: 150px;
        background: #FFF;

    }

    .sidenav {
        height: 100%;
        background-color: #000;
        overflow-x: hidden;
        padding-top: 20px;
    }


    .main {
        padding: 0px 10px;
    }

    @media screen and (max-height: 450px) {
        .sidenav {padding-top: 15px;}
    }

    @media screen and (max-width: 450px) {
        .login-form{
            margin-top: 10%;
        }

        .register-form{
            margin-top: 10%;
        }
    }

    @media screen and (min-width: 768px){
        .main{
            margin-left: 40%;
        }

        .sidenav{
            width: 40%;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
        }

        .login-form{
            margin-top: 80%;
        }

        .register-form{
            margin-top: 20%;
        }
    }


    .login-main-text{
        margin-top: 20%;
        padding: 60px;
        color: #fff;
    }

    .login-main-text h2{
        font-weight: 300;
    }

    .btn-black{
        background-color: #000 !important;
        color: #fff;
    }

</style>
<body>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<!------ Include the above in your HEAD tag ---------->
<div class="sidenav">
    <div class="login-main-text">
        <h2>E-COMMERCE<br>Register Page</h2>
        <p>Login or register from here to access.</p>
    </div>
</div>
<div class="main">
    <div class="col-md-6 col-sm-12">
        <div class="login-form">
            <span style="color: #ff0000">${error}</span>
            <form action="register" method="post">
                <div class="form-group">
                    <label>First Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Your Name" required>
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" placeholder="Email" required>
                    <label>Username</label>
                    <input type="text" class="form-control" name="username" placeholder="Username" required>
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                    <label>Confirm Password</label>
                    <input type="password" class="form-control" name="confirmPassword" placeholder="Confirm Password" required>
                </div>
                <button type="submit"  class="btn btn-black">Register</button><br>
            </form>
        </div>
    </div>
</div>
</body>
</html>
