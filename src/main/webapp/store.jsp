<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>

<h2>PRODUCT STORE</h2>

<table>
    <tr>
        <th>ID</th>
        <th>NAME</th>
        <th>QUANTITY</th>
        <th>IS ACTIVE</th>
        <th>INSERT DATE</th>
        <th></th>
        <th></th>
    </tr>
    <c:forEach items="${storeList}" var="s">
        <tr>
            <td>${s.id}</td>
            <td>${s.name}</td>
            <td>${s.quantity}</td>
            <td>${s.status}</td>
            <td>${s.insertDate}</td>
            <td><i onclick="openEditModal(${s.id})" class="fa fa-pencil-square-o" aria-hidden="true" style="color: green"></i></td>
            <td><i onclick="openDeleteModal(${s.id})" class="fa fa-trash" aria-hidden="true"></i>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
