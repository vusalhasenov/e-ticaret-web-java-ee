<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    function closeModal(){
        $('#storeEditModalId').modal('hide');
    }

    $(function() {
        <%--loadProductType('editProductTypeId', false);--%>
        <%--$('#editProductTypeId').val(${product.productTypeId});--%>
        <%--loadProduct(${product.productTypeId}, 'editProductId', false);--%>
        <%--$('#editProductId').val(${product.productId})--%>
        $('#editQuantityId').val(${product.quantity})
    });
</script>

<script>
    $('#editProductId').val(1);
</script>

<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Edit Store</h5>
</div>
<input type="hidden" id="editStoreId" value="${product.id}">
<div class="modal-body">
<%--    <div class="mb-3">--%>
<%--        <select   class="form-control" aria-label="product type" id="editProductTypeId" onchange="loadProduct($('#editProductTypeId').val(), 'editProductId')"></select>--%>
<%--    </div>--%>

<%--    <div class="mb-3">--%>
<%--        <select class="form-control" aria-label="product" id="editProductId" ></select>--%>
<%--    </div>--%>
    <h4>UPDATE QUANTITY</h4>
    <div class="mb-3">
        <input type="number" class="form-control" id="editQuantityId" placeholder="quantity">
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" onclick="closeModal()">Close</button>
    <button type="button" class="btn btn-primary" onclick="saveEditStore()">Save</button>
</div>