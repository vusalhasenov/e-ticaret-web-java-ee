<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="modal-header">
    <h5 class="modal-title">DELETE STORE</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
<%--        <span aria-hidden="true">&times;</span>--%>
    </button>
</div>
<input type="hidden" id="deleteModalStoreInputId">
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCEL</button>
    <button type="button" class="btn btn-primary" onclick="deleteStore($('#deleteModalStoreInputId').val())" id="deleteStoreId">DELETE</button>
</div>

