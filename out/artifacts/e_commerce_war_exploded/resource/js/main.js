function loadStore(){
    console.log("in loading method...")
    $.ajax({
        url:'store',
        method:'GET',
        success: function (result){
            $("#storeTableId").html(result)
        },
        error: function (result){

        }
    })

}

function openStoreModal(){
$('#storeModalId').modal('show');
}

function loadProductType(id, isAsync){
    var async = true;
    if (isAsync != undefined && isAsync != null){
        async = isAsync;
    }
    loadData('category','GET',id,isAsync)
}

function loadProduct(categoryId, productId, isAsync){
    var async = false;
    if (isAsync != undefined && isAsync != null){
        async = isAsync;
    }
    loadData('product?categoryId='+categoryId,'GET', productId, async);
}



function loadData(url ,method ,id , isAsync) {
    console.log("in loading method...")
    $.ajax({
        url: url,
        method: method,
        success: function (result){
            $("#"+id).html(result)
        },
        error: function (result){

        }
    })}

    function saveStore(){
    var data = {
        'productId': $('#productId').val(),
        'quantityId': $('#quantityId').val(),
    };
    const url = 'store';
    $.ajax({
        type: 'POST',
        url:url,
        data:data,
        dataType: 'html',
        success :function (){
            closeStoreModal();
            loadStore();
        },
        error:function (data){
            alert(data);
        }
    })
}

function saveEditStore(){
    var data = {
        'id': $('#editStoreId').val(),
        'quantityId': $('#editQuantityId').val(),
    };
    const url = 'edit-store';
    $.ajax({
        type: 'POST',
        url:url,
        data:data,
        dataType: 'html',
        success :function (){
            closeEditStoreModal();
            loadStore();
        },
        error:function (data){

        }
    })
}
// $(document).ready(function () {
//     $('#saveStoreId').click(function () {
//         var productId = $('#productId').val()
//         var quantity = $('#quantityId').val()
//         $.post('store', {'productId': productId, 'quantityId': quantity}, function (data) {
//             closeStoreModal();
//             loadStore();
//             loadStore();
//         })
//     })
// })
//
//     $(document).ready(function () {
//         alert("Salam");
//         $('#saveEditStoreId').click(function () {
//             aler
//             var storeId = $('#editStoreId').val();
//             var quantity = $('#editQuantityId').val();
//             $.post('edit-store', {'storeId': storeId, 'quantityId': quantity}, function (data) {
//             })
//             closeEditStoreModal();
//             loadStore();
//         })
//     })

    function closeStoreModal() {
        $('#storeModalId').modal('hide');
    }

    function closeEditStoreModal() {
        $('#storeEditModal').modal('hide');
    }

    function openEditModal(id) {
        $.ajax({
            url: 'edit-store?id=' + id,
            method: 'GET',
            success: function (result) {
                $('#storeEditModalId').html(result)
                $('#storeEditModal').modal('show');
            },
            error: function (result) {
            }
        })
    }

    function openDeleteModal(id) {
        $('#deleteModalId').modal('show');
        $('#deleteModalStoreInputId').val(id);
    }

    function deleteStore(id) {
        loadData('delete-store?id=' + id, 'PUT', id);
        $('#deleteModalId').modal('hide');
        loadStore();
        loadStore();

    }









